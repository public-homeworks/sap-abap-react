const express = require("express");
const mongoose = require("mongoose");
const shortId = require("shortid");

const app = express();
app.use(express.json());
mongoose.connect("mongodb://localhost/react-store-db", {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

const Product = mongoose.model(
  "products",
  new mongoose.Schema({
    _id: { type: String, default: shortId.generate },
    title: String,
    image: String,
    description: String,
    price: Number,
    year: String,
    availableStock: Number,
  })
);

app.get("/api/products", async (req, res) => {
  const products = await Product.find({}); //return all products
  res.send(products);
});

app.post("/api/product", async (req, res) => {
  const newProduct = new Product(req.body);
  const savedProduct = await newProduct.save();
  res.send(savedProduct);
});
app.post("/api/products", async (req, res) => {
  const newProducts = req.body.products;
  var savedProducts = [];
  newProducts.forEach(async (p) => {
    const product = new Product(p);
    savedProducts.push(product);
    await product.save();
  });
  res.send(savedProducts);
});
app.delete("/api/products/:id", async (req, res) => {
  const deletedProduct = await Product.findByIdAndDelete(req.params.id);
  res.send(deletedProduct);
});

const Order = mongoose.model(
  "order",
  new mongoose.Schema(
    {
      _id: {
        type: String,
        default: shortId.generate,
      },
      email: String,
      name: String,
      address: String,
      createdAt: Date,
      total: Number,
      cartItems: [
        {
          _id: String,
          title: String,
          price: Number,
          count: Number,
        },
      ],
    },
    {
      timeStamps: true,
    }
  )
);

app.post("/api/orders", async (req, res) => {
  if (
    !req.body.name ||
    !req.body.email ||
    !req.body.address ||
    !req.body.total ||
    !req.body.cartItems
  ) {
    return res.send({ message: "Data is required" });
  }

  const order = await Order(req.body).save();
  res.send(order);
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log("serve at http://localhost:" + port));
