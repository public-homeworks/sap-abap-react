## Demo

    ![](market-place/public/logo192.png)
    !https://media.giphy.com/media/lHGE01wF8GXGjMhQhN/giphy.gif

## 1. Introduction

    1. Review Shopping Cart Features
        1. Fully-functional Shopping Cart
        2. Instant Feedback
        3. Animated and Intuitive Design
        4. List all products
        5. Sort Products by price (higher/low)
        6. Filter products by size
        7. Open modal by click on product
        8. Add product to cart using animation
        9. Handle multiple click by adding more items
        10. Remove product
        11. Show checkout form
        12. Create order with user friendly id
        13. Admin section to see list of products
        14. Using Postman to add or remove products and orders

## 2. Tools and Technologies

    1. JavaScript
        1. Arrow functions
        2. Array functions
        3. Spread operators etc.
    2. React
        1. React-reveal
        2. React-modal
    3. Redux
        1. React-redux
        2. Redux-thunk
    4. Node
        1. Express
        2. Body-parser
        3. Environment variabiles
        4. Nodemon
    5. MongoDB runned in docker container
        1. Mongoose
        2. Shortid
    6. Docker desktop
    7. VS Code extensions
        1. ES6 Snippets
        2. ES7 React Extensions
        3. ESLint Extension
        4. Css Peek
        5. Node Debug
        6. MongoDB
    8. Chrome
        1. React Developer Tools
        2. Redux Developer Tools
    9. Bitbucket
    10. Git
    11. Postman
        1. GET
        2. POST
        3. PUT
        4. DELETE

## 3. Download, install and run

    1. Use git clone https://AGabi@bitbucket.org/AGabi/sap-abap-react.git in a cmd or powershell open where you want to download the project
    2. Open project folder in VSCode, open a Terminal and run cd .\market-place\
    3. Run npm ci (for downloading in node_modules all needed packages)
    4. Run docker run --name mongodb -p 27017:27017 mongo
    5. Install MongoDB extension for VSCode and create connection to mongodb://localhost:27017/
    4. Open another terminal and run npm run server
    5. Open another terminal and run npm start
    6. In browser you'll see the app and you can play with it

    Prerequisites: VSCode, Git, Docker Desktop, Node JS

## 4. Steps for creating the app

    1.  Create React App

        1. Open VSCode and open a Terminal window
        2. Open a specific directory where you want to create the app
        3. Type in terminal npx create-react-app market-place
        4. Remove some unused files like logos, tests files, App.css, App.js, service worker usage from index.js.
        5. Conver App.js to App.jsx and export it as a class component.
        a. Add in its render method header, main and footer
        b. Update index.css to contain some adjustments for the products grid.

    2.  Start to create React components

        1. Products component
            a. For orientative example purposes create a data.json with the serialized products that you want to display
            b. Update App.jsx to import data
            c. In App.jsx create 3 div areas: div.content {flex, wrap}, div.main {flex: 3 60rem} - products area and div.sidebar {flex: 1 20rem} - shopping cart area
            d. Create components/Products.jsx
            e. Add it to div.main and set up products props and actions that can performed on a product(like add to cart - for now with no implementation)
            f. Create the list of products in render from Products.jsx using <ul></ul>{flex, center, wrap, padding:0, margin:0, style:none}.Each ul element has the product id from data. Map them using this.props.products.map(p => li.key={p.\_id} {flex, p:1, m:1, none,h:47}
            g. Set up how a product from a list will be displayed using a <div></div> {flex, column, space-between, h: 100%}. Also set the image of the product a href="#" > img {max-witdh, max-height : 37} and if there are no resources display the item title. Display product price at same level with Add To Cart buton using another <div></div> inside the one above.
        2. Filter component
            a. Create components/Filter.jsx
            b. Add it above Products component in App.jsx
            c. In Filter render() method add following in order to have a sorting drop&down and a display/show one:
            - the bigger <div></div> {flex, wrap, padding, margin: 1rem, border-bottom: .1rem}
            - show total number of products existent in data.json
            - right after it show the sort drop&down with 3 options Latest (published), Lowest and Highest (price) (it's a <select> with <option>). Create a method to sort products and display them in real time sorted.
            - add a show drop&down (also <select> with <option>) in order to display certain number of products from the list: like display the first 10 or 20 and so on..
        3. Cart component in right sidebar on App.jsx -> should contain a picture of product, its title, price, a count if you want more than one of same kind, a Remove button and a Proceed Order button
            a. In Products.jsx the Add To Cart button click should point out to App.jsx addToCart method so you should have something like onClick={this.props.addToCart} and in <Products addToCart={this.addToCart(item)}> from main div. So this will establish bi-directional communication between parent App.jsx and child component.
            b. Add to App.jsx state (in class constructor) a field named cartItems and set its value to [].
            c. For addToCart(product) implementation from App.jsx do following:
                - keep in a constant variabile cartItems.slice() //clone copy
                - check if the product has been already in cart, and if yes only increase its count (that means handling multiple clicks and usage of spread operator)
            d. Create components/Cart.jsx and add the list of products if exists or your cart is empty message if not, a Proceed Order button and the total price of cart products. Also here will be added the checkout form.
            e. Add <Cart> in sidebar; bind cartItems, createOrder - behind Proceed Order and removeFromCart - behind Remove button.
        4. Checkout Form
            a. Make cart items persistent on refreshing the page using localStorage. So use localStorage in App.js constructor to load the cart items (JSON.parse) and also in addToCart(product) at the end of method.
            b. Handle click on Proceed Order button to set up a variable named showCheckout to true. Based on its value show or hide the checkout form in Cart.jsx (conditional rendering).
            c. Define a handleInput method for checkout form to get the name, address and e-mail of client.
            d. Add a checkout button (type="submit") in checkout form, handle onSubmit on form in createOrder method in Cart.jsx. This method will create an order object and pass it to App.jsx parent to handle it (createOrder it's implemented in App.jsx).
        5. Add Modal and Animation
            a. Install react-reveal using npm
            b. Create fade effect from bottom for products
            c. Create fade left effect when a product is added to cart
            d. Create fade right effect for showing checkout form
            e. Open Modal by click on product image
            f. Install react-modal via npm
            g. In Products.jsx import Modal, set state to null on a product, define openModal and closeModal in class, create zoom effect for Modal, style product details shown in modal.

    3.  Create backend server using Node JS.

        1. Install nodemon package globally (npm i -g nodemon or npm i --save-dev -g nodemon if you have problems with running the server using npm run server)
        2. Add new file named server.js in project's root
        3. Install express mongoose and shortid (npm i express body-parser mongoose shortid)
        4. Install MongoDB or use it from Docker container (docker run --name mongodb -p 27017:27017 mongo)
        5. Add in server.j following:
            a. app = express()
            b. app.use(express.json()) - in request body you will be able to send json objects (contains body-parser for express >= 4.16.0)
            c. mongoose.connect([db_name], {params})
        6. Create a Product database model (same as data.json)
        7. Create api get method for get all products (app.get("/api/products"))
        8. Send request to server via Postman
        9. Implement also post and delete for products (app.post("/api/products") and app.delete("/api/products/:id"))
        10. Install VSCode MongoDB extension and connect to the mongodb(mongodb://localhost/) image which runs in docker container. When you start the server, then mongoose will create the database.

    4.  Add Redux to Products component (for connection between client and server)

        1. Install redux using (npm i redux react-redux redux thunk)
        2. Create types (types.js and export + define constant FETCH_PRODUCTS)
        3. Create actions/productActions.js
        4. Declare fetchProducts method for dispatching an action that will bring products from server
        5. Create reducers in reducers/productReducers.js
        6. Define switch case for FETCH_PRODUCTS action type and return payload which is the server response data.
        7. Create Redux store (store.js)
            a. Import redux methods and redux-thunk
            b. Set an initial empty state constant which will be used when creating the store
            c. Create store
            d. Import productReducers
            e. Combine reducers
            f. Use middleware and thunk to handle async operations/requests to server
        8. Use store in App.js
        9. Wrap all components inside Provider
        10. Connect Provider to store
        11. Connect products
        12. Import fetchProducts from productActions.js
        13. Export connected component for Products using connect method from react-redux (components/Products.js)
            a. Show Loading... message if no products are available when opening app
            b. Connect props: products to payload.items
            c. Connect actions: fetchProducts
        14. Update package.json to contain proxyServer on http://127.0.0.1:5000 in order to not be necessary in each request to put a prefix with the name of the server

    5.  Add Redux to Filter component

        1. Types.js and create two constants SORT_PRODUCTS and FETCH_CHUNK_OF_PRODUCTS
        2. Create sortProducts using App.js sortProducts logic in actions/productAction.js
        3. Create showProducts using App.js showProducts logic in actions/productAction.js
        4. Create SORT_PRODUCTS and FETCH_CHUNK_OF_PRODUCTS cases in reducers/productReducers
        5. Export connected component for Filter using connect method from react-redux (components/Filter.js)
            a. Connect props: chunkSize, sortValue, items and filteredItems
            b. Connect actions: showProducts and sortProducts
            c. Show Loading...message if no filteredProducts
            d. From App.js remove Filter props

    6.  Add Redux to Cart component

        1. Create constants in types.js named ADD_TO_CART and REMOVE_FROM_CART
        2. Create actions/cartActions.js with addToCart and removeFromCart exported methods
        3. Create reducers/cartReducers.js with ADD_TO_CART and REMOVE_FROM_CART cases
        4. Export connected component for Cart using connect method from react-redux (components/Cart.js)
            a. Connect props: cartItems
            b. Connect actions: removeFromCart
        5. In Product.js connect action addToCart
        6. App.js remove Cart properties
        7. In store.js set initial cartItems to localStorage and combine the cartReducer with productsReducer.

    7.  Create Order

        1. Create backend
        2. Server.js
        a. Create order model with mongoose
            b. Create get("/api/orders")
            c. Create post("/api/orders")
            d. Create delete("/api/orders")
        3. Frontend
            a. Create in types.js CLEAR_ORDER, CLEAR_CART, CREATE_ORDER constans
        4. Create actions/orderActions.js with createOrder(order), clearOrder()
        5. Create reducers/orderReducers.js with CLEAR_ORDER, CLEAR_CART, CREATE_ORDER cases
        6. Update Cart component, connect order, add createOrder, clearOrder, and for the order form add the following: onSubmit={this.createOrder}
